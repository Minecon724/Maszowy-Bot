import discord
from discord.ext import commands
import asyncio
import requests as r

class Image:
    def __init__(self, client):
        self.client = client

    @commands.command(pass_context=True)
    async def dog(self, ctx):
        ''' Wysyła zdjęcie psa. '''
        msg = await self.client.say(":mag:")
        embed = discord.Embed(title="Znalazłam psa:")
        dog = r.get("https://dog.ceo/api/breeds/image/random").json()['message']
        embed.set_image(url=dog)
        embed.set_footer(text=dog)
        await self.client.edit_message(msg, embed=embed)
    
    @commands.command(pass_context=True)
    async def rasa(self, ctx, *, rasa:str):
        ''' Wysyła zdjęcie rasy psa. '''
        msg = await self.client.say(":mag:")
        try:
            dog = r.get("https://dog.ceo/api/breed/{}/images/random".format(rasa)).json()['message']
            embed = discord.Embed(title="Znalazłam psa:")
            embed.set_image(url=dog)
            embed.set_footer(text=dog)
            await self.client.edit_message(msg, embed=embed)
        except:
            await self.client.edit_message(msg, "Niestety, nic nie znalazłam. Hmmm... :thinking: być może nie ma takiej rasy. :dog2:")

    @commands.command(pass_context=True)
    async def rasy(self, ctx):
        ''' Wyświetla dostępne rasy psów. '''

def setup(client):
    client.add_cog(Image(client))