import discord
from discord.ext import commands
import asyncio

class Util:
    def __init__(self, client):
        self.client = client
    
    @commands.command(pass_context=True, aliases=['wyrzuc'])
    async def wyrzuć(self, ctx, user:discord.Member):
        ''' Wyrzuca użytkownika. '''
        try:
            await self.client.kick(user)
            await self.client.add_reaction(ctx.message, '✅')
        except:
            await self.client.add_reaction(ctx.message, '❎')
    
    @commands.command(pass_context=True)
    async def zbanuj(self, ctx, user:discord.Member):
        ''' Banuje użytkownika. '''
        try:
            await self.client.ban(user)
            await self.client.add_reaction(ctx.message, '✅')
        except:
            await self.client.add_reaction(ctx.message, '❎')
    
    @commands.command(pass_context=True)
    async def info(self, ctx, user:discord.User=None):
        ''' Wyświetla informacje o użytkowniku. '''
        if user is None:
            user = ctx.message.author
        embed = discord.Embed(title="Użytkownik {}".format(user.display_name), description="Informacje", color=user.color)
        embed.add_field(name="Wyświetlam informacje o:", value="{}".format(user.name), inline=True)
        embed.add_field(name="Użytkownik jest:", value="{}".format(user.status), inline=True)
        embed.add_field(name="ID użytkownika:", value="{}".format(user.id), inline=True)
        embed.add_field(name="Użytkownik dołączył do serwera w:", value="{}".format(user.joined_at), inline=True)
        embed.add_field(name="Użytkownik stworzył konto w:", value="{}".format(user.created_at), inline=True)
        embed.add_field(name="Najwyższa rola użytkownika na tym serwerze:", value="{}".format(user.top_role), inline=True)
        embed.set_thumbnail(url=user.avatar_url)
        embed.set_footer(text=user)
        await self.client.say(embed=embed)

    @commands.command(pass_context=True)
    async def Serwer(self, ctx):
        embed = discord.Embed(title="{}".format(ctx.message.server.name), description="Informacje")
        embed.add_field(name="ID serwera:", value="{}".format(str(ctx.message.server.id)), inline=True)
        embed.add_field(name="Ilość ról:", value="{}".format(str(len(ctx.message.server.roles))), inline=True)
        embed.add_field(name="Ludzi na serwerze:", value="{}".format(str(len([member for member in ctx.message.server.members if not member.bot]))), inline=True)
        embed.add_field(name="Botów na serwerze:", value="{}".format(str(len([member for member in ctx.message.server.members if member.bot]))), inline=True)
        embed.set_thumbnail(url=ctx.message.server.icon_url)
        embed.set_footer(text=ctx.message.server.name)
        await self.client.say(embed=embed)

def setup(client):
    client.add_cog(Util(client))