# -*- coding: utf-8 -*-

wersja = "1.0"

from time import monotonic
f = monotonic()
import discord
from discord.ext import commands
import asyncio
import requests as r
from math import floor
import psutil
import datetime
import os

client = commands.Bot(command_prefix="m:")

cogs = ['image', 'util']
exceptions = False
notloaded = []

@client.event
async def on_ready():
    print("Bot gotowy! Test pingu...")
    ping1 = monotonic()
    r.get("https://discordapp.com:443")
    ping11 = monotonic()
    if not exceptions:
        msg = "Nie wystąpiły żadne błędy podczas uruchamiania."
    else:
        msg = "Podczas uruchamiania wystąpił błąd. Nie udało się załadować rozszerzeń: {}.".format(notloaded)
    await client.send_message(discord.Object(id='521252141407404033'), "----------------------------\nHau! Jestem gotowa!\nMoja nazwa {}\nCzas uruchomienia: {}ms\nPołączenie z Discordem: {}ms\nLiczba rozszerzeń: {}\n{}".format(str(client.user), str(floor((monotonic() - f) * 1000)), str(floor((ping11 - ping1) * 1000)), str(len(cogs)), msg))
    await client.change_presence(game=discord.Game(name=":dog: - {} serwer".format(str(len(client.servers)))))
    global odebrane
    odebrane = 0
    global odebranemsg
    odebranemsg = await client.send_message(discord.Object(id='521252141407404033'), "Odebranych wiadomości: 0.")

if __name__ == '__main__':
    for cog in cogs:
        try:
            client.load_extension(cog)
        except Exception as e:
            print("{} nie może zostać załadowany. {}".format(cog, e))
            exceptions = True
            notloaded.append(cog)

@client.event
async def on_message(message):
    global odebrane
    odebrane += 1
    await client.edit_message(odebranemsg, "Odebranych wiadomości: {}.".format(str(odebrane)))
    message.content = message.content.lower()
    await client.process_commands(message)

async def uptime():
    await client.wait_until_ready()
    global uruchomiony
    uruchomiony = str(datetime.datetime.now())
    hours = 0
    minutes = 0
    seconds = 0
    global uptimemsg
    uptimemsg = "0:0:0"
    while not client.is_closed:
        await asyncio.sleep(1)
        seconds += 1
        if seconds == 60:
            minutes += 1
            seconds = 0
        if minutes == 60:
            hours += 1
            minutes = 0
        if hours == 0:
            uptimemsg = str(minutes) + ":" + str(seconds)
        else:
            uptimemsg = str(hours) + ":" + str(minutes) + ":" + str(seconds)

client.loop.create_task(uptime())

@client.command(pass_context=True)
async def statystyki(ctx):
    global uptimemsg
    global uruchomiony
    embed = discord.Embed(title="Statystyki bota:")
    embed.add_field(name="Prefix", value="m:", inline=False)
    embed.add_field(name="Wersja", value="{}".format(wersja), inline=False)
    embed.add_field(name="Jestem online", value="{}".format(uptimemsg), inline=False)
    embed.add_field(name="Uruchomiony od", value="{}".format(uruchomiony), inline=False)
    embed.add_field(name="Czas uruchomienia", value="{}ms".format(str(floor((monotonic() - f) * 1000))), inline=False)
    embed.add_field(name="Strefa czasowa", value="{}".format(str(datetime.datetime.now(datetime.timezone.utc).astimezone().tzname())), inline=False)
    embed.add_field(name="Obciążenie procesora", value="{}%".format(str(psutil.cpu_percent())), inline=False)
    embed.add_field(name="Twórca", value="Minecon724#2556 (dla Maszowy Serwer)", inline=False)
    embed.add_field(name="Dołącz do serwera!", value="https://discord.gg/EAKqxAw", inline=False)
    await client.say(embed=embed)

client.run(os.environ.get("TOKEN"))